﻿using System.Collections.Generic;


namespace WebApplication1.Models
{
    public class Movie
    {
        public int ID { get; set; }
        public int Year { get; set; }
        public string Name { get; set; }

        public string Actors { get; set; }
    }
}