﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.DAL;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private TutorialContext db = new TutorialContext();
        public ActionResult Index()
        {
            return View();
        }
            
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Generate()
        {
            var People = new List<Person>
            {
                new Person {FirstName="Test",LastName="Wow",Age=12,Gender=Person.Genders.Male,Description="Ohla I'm a test"},
                new Person {FirstName="Test2",LastName="Aww",Age=12,Gender=Person.Genders.Male,Description="Hello I'm a test"},
               

            };
            var Movies = new List<Movie>
            {
                new Movie {Year=2030,Name="Titanic",Actors="Tony Deng"},
                new Movie {Year=2030,Name="The God Father",Actors="Michael Corleone"},
            };

            People.ForEach(s => db.People.Add(s));
            Movies.ForEach(s => db.Movie.Add(s));
            db.SaveChanges();
            return View();
        }
    }
}